import scala.io.StdIn
import java.net._


object TestServer {
  val port = 4000
  val packetSize = 512
  val id = 101

  val response = "ACK 2.04 " + id
  val sendData = response.getBytes

  def main(args: Array[String]): Unit = {
    val serverSocket = new DatagramSocket(port)

    def receive(): String = {
      val receiveData = new Array[Byte](packetSize)
      val receivePacket = new DatagramPacket(receiveData, receiveData.length)
      serverSocket.receive(receivePacket)

      val IPAddress = receivePacket.getAddress()
      val portReceived = receivePacket.getPort()
      val sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, portReceived)
      serverSocket.send(sendPacket)

      "Connection from: " + IPAddress +":"+ portReceived+ " > " + new String(receivePacket.getData())
    }

    while(true)
     {
        Console.println(receive())
     }
  }
}
